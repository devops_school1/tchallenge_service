#!/bin/bash

. smoke_utils.sh

smoke_url_prefix $1

smoke_header "Content-Type:application/json"

PAYLOAD='{"method":"PASSWORD","email":"user@user.com","password":"12345"}'

smoke_json_ok "/security/tokens/" $PAYLOAD
    smoke_assert_body "payload"
    smoke_assert_body "PARTICIPANT"

smoke_report
