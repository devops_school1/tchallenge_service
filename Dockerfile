# take base from gradle image (from docker hub) and name it "build" for further reference
FROM gradle:jdk8 AS build
# make gradle owner (linux command) and copy new files or directories from <src> (.)
# and adds them to the filesystem of the container at the path <dest> (/home/gradle/src)
COPY --chown=gradle:gradle source /home/gradle/src
# make workdir that we copied to
WORKDIR /home/gradle/src
# make build
RUN gradle build

# take openjdk base image
FROM openjdk:8
# inform Docker that the container listens on the specified network ports at runtime
EXPOSE 4567
RUN mkdir /app

# create image from this docker file: docker build -f DockerFile (file to use) -t tchallenge-service-multi-stage (image name) . (current directory to use)
# run created image: docker run -p 4567:4567 (match port on machine to port on container) tchallenge-service-multi-stage (image name)

COPY --from=build /home/gradle/src/build/libs/*.jar /app/tchallenge-service.jar
ENTRYPOINT ["java", "-jar", "app/tchallenge-service.jar"]